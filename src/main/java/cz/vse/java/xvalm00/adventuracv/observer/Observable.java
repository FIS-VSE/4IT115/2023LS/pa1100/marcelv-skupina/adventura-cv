package cz.vse.java.xvalm00.adventuracv.observer;

public interface Observable {

    void register(Observer observer);
    void unregister(Observer observer);
    void notifyObservers();

}
