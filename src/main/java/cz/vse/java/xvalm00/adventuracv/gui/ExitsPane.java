package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

/**
 * Class ExitsPane - a JavaFX component that displays all possible exits from the current location.
 *
 * @author YourName
 * @version 1.0
 */
public class ExitsPane extends AnchorPane implements Observer {

    private Hra hra = Hra.getSingleton();
    private HerniPlan herniPlan = hra.getHerniPlan();
    private ListView<String> list;

    /**
     * Constructor for objects of class ExitsPane.
     */
    public ExitsPane() {
        herniPlan.register(this);

        list = new ListView<>();
        update();

        this.getChildren().add(list);
    }

    /**
     * Updates the list of exits from the current location.
     */
    @Override
    public void update() {
        ObservableList<String> exits = FXCollections.observableArrayList();
        Prostor currentLocation = herniPlan.getAktualniProstor();

        if (currentLocation != null) {
            for (Prostor exit : currentLocation.getVychody()) {
                exits.add(exit.getNazev());
            }
        }

        list.setItems(exits);
    }

    public ListView<String> getList() {
        return list;
    }
}
