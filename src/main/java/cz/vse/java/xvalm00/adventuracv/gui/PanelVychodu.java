package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import java.util.Collection;

public class PanelVychodu {

    Hra hra = Hra.getSingleton();
    HerniPlan herniPlan = hra.getHerniPlan();
    ListView<String> listView = new ListView<>();

    public PanelVychodu() {
        aktualizuj();
    }

    private void aktualizuj() {
        ObservableList<String> listViewItems = listView.getItems();
        listViewItems.clear();
        Collection<Prostor> vychody = herniPlan.getAktualniProstor().getVychody();
        for (Prostor prostor : vychody) {
            listViewItems.add(prostor.getNazev());
        }

    }

    public ListView<String> getListView() {
        return listView;
    }
}
