package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.Batoh;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.observer.Observer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

import java.util.Set;

public class PanelBatohu implements Observer {

    private final FlowPane flowPane = new FlowPane();
    private final Batoh batoh = Hra.getSingleton().getBatoh();

    public PanelBatohu() {
        init();
        aktualizuj();
        batoh.register(this);
    }

    private void init() {
        flowPane.setMinWidth(200);
        flowPane.setMaxWidth(200);
    }

    public FlowPane getFlowPane() {
        return flowPane;
    }

    private void aktualizuj() {
        flowPane.getChildren().clear();

        Set<String> mnozinaVeci = batoh.getMnozinaVeci();
        for (String nazevVeci : mnozinaVeci) {
            Image image = new Image(PanelBatohu.class.getResourceAsStream(nazevVeci + ".jpeg"), 100.0, 100.0, false,
                    false);
            ImageView imageView = new ImageView(image);
            flowPane.getChildren().add(imageView);
        }
    }

    @Override
    public void update() {
        aktualizuj();
    }
}
