package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.observer.Observer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class MapaHry implements Observer {

    private final IHra hra = Hra.getSingleton();
    private Circle circle = new Circle(10, Color.RED);
    private final double defaultMarginLeft = 200.0;

    private int counter = 0;

    private AnchorPane anchorPane = new AnchorPane();

    public MapaHry() {
        this.hra.getHerniPlan().register(this);
        init();
        aktualizuj();
    }

    private void aktualizuj() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        Double posLeft = aktualniProstor.getPosLeft();
        Double posTop = aktualniProstor.getPosTop();
        AnchorPane.setTopAnchor(circle, posTop);
        AnchorPane.setLeftAnchor(circle, posLeft + defaultMarginLeft);
    }

    private void init() {
        nactiObrazek();
    }

    private void nactiObrazek() {
        Image image = new Image(MapaHry.class.getResourceAsStream("herniPlan.png"), 400.0, 250.0, false, false);
        ImageView imageView = new ImageView(image);
        anchorPane.getChildren().addAll(imageView, circle);
        AnchorPane.setLeftAnchor(imageView, defaultMarginLeft);
    }

    public AnchorPane getAnchorPane() {
        counter ++;
        return anchorPane;
    }

    @Override
    public void update() {
        aktualizuj();
    }
}
