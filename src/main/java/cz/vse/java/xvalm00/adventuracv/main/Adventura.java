package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.MapaHry;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Adventura extends Application {

    IHra hra = Hra.getSingleton();
    // změna

    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("-text")) {
            TextoveRozhrani.main(args);
        } else {
            Application.launch(args);
        }
    }

    @Override
    public void start(Stage stage) {
        BorderPane borderPane = new BorderPane();

        TextField prikazovePole = pripravPrikazovouRadku(borderPane);

        TextArea textArea = new TextArea();
        borderPane.setCenter(textArea);

        AnchorPane anchorPane = pripravPlanekHry();
        borderPane.setTop(anchorPane);

        PanelVychodu panelVychodu = new PanelVychodu();
        borderPane.setRight(panelVychodu.getListView());

        PanelBatohu panelBatohu = new PanelBatohu();
        borderPane.setLeft(panelBatohu.getFlowPane());

        nastavPrikazovePoleATextArea(prikazovePole, textArea);

        Scene scene = new Scene(borderPane, 800, 500);
        stage.setScene(scene);
        stage.show();
    }

    private static TextField pripravPrikazovouRadku(BorderPane borderPane) {
        TextField prikazovePole = new TextField();
        Label label = new Label("Zadej příkaz: ");
        label.setFont(Font.font("Arial", FontWeight.BOLD, 14.0));
        label.setAlignment(Pos.BASELINE_CENTER);
        HBox hBox = new HBox();
        hBox.getChildren().addAll(label, prikazovePole);
        hBox.setAlignment(Pos.BASELINE_CENTER);
        borderPane.setBottom(hBox);
        return prikazovePole;
    }

    private AnchorPane pripravPlanekHry() {
        MapaHry herniPlocha = new MapaHry();
        AnchorPane anchorPane = herniPlocha.getAnchorPane();
        return anchorPane;
    }

    private void nastavPrikazovePoleATextArea(TextField prikazovePole, TextArea textArea) {
        textArea.setEditable(false);
        textArea.setText(hra.vratUvitani());

        prikazovePole.setOnAction(actionEvent -> {
            String vstup = prikazovePole.getText();
            String odpoved = hra.zpracujPrikaz(vstup);
            textArea.appendText("\n" + odpoved + "\n");

            prikazovePole.setText("");
        });
    }
}
